@extends('layouts.app')

@section('content')
	<h1>{{$post->title}}</h1>
	<small>Written on {{$post->created_at}}</small>

	<div class="mt-4">
		{{$post->body}}

	@if (!Auth::guest())
		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#commentModal">Post Comment</button>		
	@endif

	@if (count ($post->comments) > 0)
		<h5 class="mt-5">Comments</h5>
		<div class="card">
			<ul class="list-group list-group-flush">
				@foreach ($post->comments as $comment)
					<p class="text-center">{{$comment->content}}</p>
					<p class="text-right">Posted by: {{$comment->user->name}}</p>
					<p class="text-right">Posted on: {{$comment->user->created_at}}</p>
				@endforeach
			</ul>
		</div>
	@endif
        
	</div>

	<!-- Add New Comment Modal Start -->

<div id="commentModal" class="modal fade" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">            
            <div class="modal-body p-10">
				<div class="text-right p-10">
					<a data-dismiss="modal" href="javascript:;">X</a>
				</div>
				<div class="justify-center items-center px-10 py-10">
					<form method="post" action="/posts/{{$post->id}}/comment" enctype="multipart/form-data">
                    @csrf
                        <div class="form-group mt-3">
                            <label for="content" class="form-label">Content:</label> 
                            <input id="content" name="content"  type="text" class="form-control" Required > 
                        </div>
                        <button type="submit" class="btn btn-primary mt-5">Submit</button> 
                    </form>                                       
				</div>
            </div>
        </div>
    </div>
</div>
<!-- Add Comment Modal End -->

@endsection
