<?php

Route::get('/', 'PageController@index');
// Route::get('/hello', 'PageController@hello');
Route::get('/about', 'PageController@about');
Route::get('/services', 'PageController@services');

Route::resource('/posts', 'PostController');
Route::post("/posts/{id}/comment', 'PostController@comment");

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
